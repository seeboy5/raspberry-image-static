#!/usr/bin/env bash

set -e

if [ "x" == "x$1" ]; then
    echo "usage $0 <full image>"
    exit 1
fi

# setting PATH for security
# weird path, but it reflect the one in docker - bundle thing relates to ruby bundler
PATH="/usr/local/bundle/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin"

MNTDIR="mnt"
FULL_IMG=$1

# ref: https://blog.tinned-software.net/mount-raw-image-of-entire-disc/
echo "image filetype"
file $FULL_IMG

# extracting geometry from image file
sector_size=$(fdisk -l $FULL_IMG | grep "Sector size" | cut -d' ' -f 4)
offset_root=$(fdisk -l $FULL_IMG | grep "img2" | awk -F'\ *' '{print $2}')
sectors_root=$(fdisk -l $FULL_IMG | grep "img2" | awk -F'\ *' '{print $4}')

offset_boot=$(fdisk -l $FULL_IMG | grep "img1" | awk -F'\ *' '{print $2}')
sectors_boot=$(fdisk -l $FULL_IMG | grep "img1" | awk -F'\ *' '{print $4}')
echo "- Root partition at $offset_root*$sector_size and boot partition at $offset_boot*$sector_size"

echo "- loop device usage"
losetup -l

echo "- create nodes in /dev if not already exists"
if [ ! -e /dev/loop0 ]; then
 for i in $(seq 0 9); do
    mknod -m 0660 "/dev/loop$i" b 7 "$i"
 done
fi

echo "- loop devices in /dev"
ls -al /dev/loop*

# we sometimes get the error
# losetup: <filename> failed to set up loop device: Resource temporarily unavailable 117
# solution adapted from https://gitlab.com/openflexure/pi-gen/blob/8f2539d4f701c623d888a435da418991a411ef5f/export-image/prerun.sh#L44

LOOPDEV_ROOT=$(losetup -f)
echo "- root partition on $LOOPDEV_ROOT"
until losetup $LOOPDEV_ROOT --offset $(($offset_root*$sector_size)) --sizelimit $(($sectors_root*$sector_size)) $FULL_IMG; do
	echo "- Error in losetup for root device (on $LOOPDEV_ROOT). Retrying forever..."
	sleep 5
done
mount $LOOPDEV_ROOT $MNTDIR

LOOPDEV_BOOT=$(losetup -f)
echo "- boot partition on $LOOPDEV_BOOT"
until losetup $LOOPDEV_BOOT --offset $(($offset_boot*$sector_size)) --sizelimit $(($sectors_boot*$sector_size)) $FULL_IMG; do
	echo "- Error in losetup for boot device (on $LOOPDEV_BOOT). Retrying forever..."
	sleep 5
done
mount $LOOPDEV_BOOT $MNTDIR/boot
