σ
ρ΄\c           @` s%  d  d l  m Z m Z m Z e Z d Z d  d l Z d  d l	 Z	 d  d l
 Z	 d  d l Z d  d l Z d  d l m Z d  d l m Z d  d l m Z d  d l m Z d  d l m Z d  d	 l m Z m Z y d  d
 l m Z Wn* e k
 r
d  d l m Z e   Z n Xd e f d     YZ  d S(   i    (   t   absolute_importt   divisiont   print_functions½  
    author: Maykel Moya <mmoya@speedyrails.com>
    connection: proot
    short_description: Interact with local proot
    description:
        - Run commands or put/fetch files to an existing proot on the Ansible controller.
    version_added: "1.1"
    options:
      remote_addr:
        description:
            - The path of the proot you want to access.
        default: inventory_hostname
        vars:
            - name: ansible_host
      executable:
        description:
            - User specified executable shell
        ini:
          - section: defaults
            key: executable
        env:
          - name: ANSIBLE_EXECUTABLE
        vars:
          - name: ansible_executable
N(   t	   constants(   t   AnsibleError(   t   is_executable(   t   shlex_quote(   t   to_bytes(   t   ConnectionBaset   BUFSIZE(   t   display(   t   Displayt
   Connectionc           B` s   e  Z d  Z d Z e Z e e j  j	 d  Z
 d Z d   Z d   Z e j d  Z d e d  Z d   Z d	   Z d
   Z d   Z RS(   s    Local proot based connections t   proott   sut   rootc         O` sθ   t  t |   j | | | |  |  j j |  _ t j j |  j  sY t	 d |  j   n  t j j
 |  j d  } t |  p t j j |  o t j j |  s· t	 d |  j   n  t j j d  |  _ |  j sδ t	 d   n  d  S(   Ns   %s is not a directorys   bin/shs7   %s does not look like a prootable dir (/bin/sh missing)R   s   proot command not found in PATH(   t   superR   t   __init__t   _play_contextt   remote_addrt	   proot_dirt   ost   patht   isdirR   t   joinR   t   lexistst   islinkt	   distutilst   spawnt   find_executablet	   proot_cmd(   t   selft   play_contextt	   new_stdint   argst   kwargst   prootsh(    (    sU   /mnt/storage/moz/git/moozer.gitlab/raspberry-image-static/connection_plugins/proot.pyR   F   s    0	c         C` sB   t  t |   j   |  j s> t j d d |  j t |  _ n  d S(   s*    connect to the proot; nothing to do here s   THIS IS A LOCAL proot DIRt   hostN(   R   R   t   _connectt
   _connectedR
   t   vvvR   t   True(   R   (    (    sU   /mnt/storage/moz/git/moozer.gitlab/raspberry-image-static/connection_plugins/proot.pyR&   _   s    	c         C` sΡ   t  j r t  j j   d n d } |  j d |  j d d d d d d d d	 d
 d d | d | g } t j d | d |  j g  | D] } t | d d ^ q } t j	 | d t
 d | d t j d t j } | S(   s.   run a command on the proot.  This is only needed for implementing
        put_file() get_file() so that we don't have to read the whole file
        into memory.

        compared to exec_command() it looses some niceties like being able to
        return the process's exit code immediately.
        i    s   /bin/shs   -rs   -wt   /s   -bs   /devs   /syss   /procs   -0s   -qs   qemu-arm-statics   -cs   EXEC %sR%   t   errorst   surrogate_or_strictt   shellt   stdint   stdoutt   stderr(   t   Ct   DEFAULT_EXECUTABLEt   splitR   R   R
   R(   R   t
   subprocesst   Popent   Falset   PIPE(   R   t   cmdR.   t
   executablet	   local_cmdt   it   p(    (    sU   /mnt/storage/moz/git/moozer.gitlab/raspberry-image-static/connection_plugins/proot.pyt   _buffered_exec_commandf   s    "	%c         C` sV   t  t |   j | d | d | |  j |  } | j |  \ } } | j | | f S(   s    run a command on the proot t   in_datat   sudoable(   R   R   t   exec_commandR=   t   communicatet
   returncode(   R   R8   R>   R?   R<   R/   R0   (    (    sU   /mnt/storage/moz/git/moozer.gitlab/raspberry-image-static/connection_plugins/proot.pyR@      s    "c         C` sC   | j  t j j  s3 t j j t j j |  } n  t j j |  S(   s   Make sure that we put files into a standard path

            If a path is relative, then we need to choose where to put it.
            ssh chooses $HOME but we aren't guaranteed that a home dir will
            exist in any given proot.  So for now we're choosing "/" instead.
            This also happens to be the former default.

            Can revisit using $HOME instead if it's a problem
        (   t
   startswithR   R   t   sepR   t   normpath(   R   t   remote_path(    (    sU   /mnt/storage/moz/git/moozer.gitlab/raspberry-image-static/connection_plugins/proot.pyt   _prefix_login_path   s    
c      
   C` sy  t  t |   j | |  t j d | | f d |  j t |  j |   } yt t	 | d d d  β } t
 j | j    j s d } n d } y& |  j d | t | f d	 | } Wn t k
 rΫ t d
   n Xy | j   \ } } Wn' t j   t d | | f   n X| j d k rJt d | | | | f   n  Wd QXWn! t k
 rtt d |   n Xd S(   s%    transfer a file from local to proot s   PUT %s TO %sR%   R+   R,   t   rbs    count=0t    s   dd of=%s bs=%s%sR.   s1   proot connection requires dd command in the proots    failed to transfer file %s to %si    s'   failed to transfer file %s to %s:
%s
%sNs$   file or module does not exist at: %s(   R   R   t   put_fileR
   R(   R   R   RG   t   openR   R   t   fstatt   filenot   st_sizeR=   R	   t   OSErrorR   RA   t	   tracebackt	   print_excRB   t   IOError(   R   t   in_patht   out_patht   in_filet   countR<   R/   R0   (    (    sU   /mnt/storage/moz/git/moozer.gitlab/raspberry-image-static/connection_plugins/proot.pyRJ      s*     	&
)c      
   C` s_  t  t |   j | |  t j d | | f d |  j t |  j |   } y |  j d | t	 f  } Wn t
 k
 r t d   n Xt t | d d d  ³ } yB | j j t	  } x) | rι | j |  | j j t	  } qΑ WWn' t j   t d | | f   n X| j   \ } } | j d	 k rUt d
 | | | | f   n  Wd QXd S(   s"    fetch a file from proot to local s   FETCH %s TO %sR%   s   dd if=%s bs=%ss1   proot connection requires dd command in the prootR+   R,   s   wb+s    failed to transfer file %s to %si    s'   failed to transfer file %s to %s:
%s
%sN(   R   R   t
   fetch_fileR
   R(   R   R   RG   R=   R	   RO   R   RK   R   R/   t   readt   writeRP   RQ   RA   RB   (   R   RS   RT   R<   t   out_filet   chunkR/   R0   (    (    sU   /mnt/storage/moz/git/moozer.gitlab/raspberry-image-static/connection_plugins/proot.pyRW   ·   s&     	
c         C` s    t  t |   j   t |  _ d S(   s.    terminate the connection; nothing to do here N(   R   R   t   closeR6   R'   (   R   (    (    sU   /mnt/storage/moz/git/moozer.gitlab/raspberry-image-static/connection_plugins/proot.pyR\   Ο   s    (   R   N(   t   __name__t
   __module__t   __doc__t	   transportR)   t   has_pipeliningt	   frozensetR1   t   BECOME_METHODSt
   differencet   become_methodst   default_userR   R&   R4   R7   R=   t   NoneR6   R@   RG   RJ   RW   R\   (    (    (    sU   /mnt/storage/moz/git/moozer.gitlab/raspberry-image-static/connection_plugins/proot.pyR   :   s   		 				(!   t
   __future__R    R   R   t   typet   __metaclass__t   DOCUMENTATIONt   distutils.spawnR   R   t   os.pathR4   RP   t   ansibleR   R1   t   ansible.errorsR   t   ansible.module_utils.basicR   t   ansible.module_utils.six.movesR   t   ansible.module_utils._textR   t   ansible.plugins.connectionR   R	   t   __main__R
   t   ImportErrort   ansible.utils.displayR   R   (    (    (    sU   /mnt/storage/moz/git/moozer.gitlab/raspberry-image-static/connection_plugins/proot.pyt   <module>   s&   